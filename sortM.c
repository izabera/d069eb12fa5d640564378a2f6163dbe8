#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

int compmon1(const char *x, const char *y) {
  struct tm thyme;
  int dx;
  char *xx,*yy;

  xx = strptime(x,"%b",&thyme);
  dx = thyme.tm_mon;
  yy = strptime(y,"%b",&thyme);
  if (!xx) return !yy ? 0 : -1;
  else if (!yy) return 1;
  else return dx==thyme.tm_mon ? 0 : dx-thyme.tm_mon;
}

int compmon2(const char *x, const char *y) {
  // slightly edited version that i ended up using in my own code
  int dx = 0, dy = 0;
  char *months = "jan" "feb" "mar" "apr" "may" "jun" "jul" "aug" "sep" "oct" "nov" "dec";
  for (int i = 1; i <= 12; i++) {
    if (strncasecmp(months, x, 3) == 0) dx = i;
    if (strncasecmp(months, y, 3) == 0) dy = i;
    months += 3;
  }
  return dx && dy ? dx - dy : !dx && !dy ? 0 : dx ? 1 : -1;
}

int main() {
  struct timespec t1, t2, t3;
  int sum1 = 0, sum2 = 0;
  char *months[] = {
    "Jan", "feb", "Mar", "apr", "May", "jun",
    "Jul", "aug", "Sep", "oct", "Nov", "dec"
  };
#define iterations 1000000

  clock_gettime(CLOCK_MONOTONIC, &t1);

  srand(1);
  for (size_t i = 0; i < iterations; i++) {
    char *mon1 = months[rand()%12];
    char *mon2 = months[rand()%12];
    sum1 += compmon1(mon1, mon2);
  }

  clock_gettime(CLOCK_MONOTONIC, &t2);

  srand(1);
  for (size_t i = 0; i < iterations; i++) {
    char *mon1 = months[rand()%12];
    char *mon2 = months[rand()%12];
    sum2 += compmon2(mon1, mon2);
  }

  clock_gettime(CLOCK_MONOTONIC, &t3);

  printf("sum1: %d sum2: %d    ", sum1, sum2);
  printf("t2 - t1 = %f    ",
      ((t2.tv_sec * 1000000000 + t2.tv_nsec) -
       (t1.tv_sec * 1000000000 + t1.tv_nsec)) / 1000000000.0);
  printf("t3 - t2 = %f\n",
      ((t3.tv_sec * 1000000000 + t3.tv_nsec) -
       (t2.tv_sec * 1000000000 + t2.tv_nsec)) / 1000000000.0);
  return 0;
}
